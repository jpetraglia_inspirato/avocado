console.log('Aloha from the change set summary!');


chrome.runtime.onMessage.addListener(function (response, sender, sendResponse) {
   console.log('css <- bg', response, chrome.runtime.lastError)
   if (response.type === "title") {
      document.title = response.title;
      sendResponse('css.listen.success');
   }
   else
      sendResponse('css.listen');
   return true
 });
