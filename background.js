chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
    console.log('Received message:', message);
    if(message.type == 'notify') {
        var failureMessage = message.failures ? (': ' + message.failures + ' failure' + (message.failures > 1 ? 's' : '')) : '';
        notify(message.status + failureMessage, message.orgName + ': ' + message.changeSetName, sendResponse);
    }
    if(message.type == 'title') {
        title(message.title);
    }
    return true;
});

// If I pass windowId with each request, that can be used to 

function title(title, sendResponse) {
    chrome.tabs.query({active: true, lastFocusedWindow: true}, function(tabs){
        console.log("bg <- chdt");
        console.log('bg -> css', tabs.length, tabs);
        chrome.tabs.sendMessage(tabs[0].id, {type: 'title', title: title}, function(response) {
            console.log('bg <- css', tabs.length, tabs);
            console.log('Fishy title response:', response, chrome.runtime.lastError);
            if(sendResponse) {
                console.log('bg <- css', tabs.length, tabs);
                sendResponse('background.title');
            }
        });  
    });
}

function notify(title, message, sendResponse) {
    var notificationId = 'fishy-notification-' + message.orgName;
    chrome.notifications.clear(notificationId, function() {
        chrome.notifications.create(
            notificationId,
            {
                type: 'basic', 
                iconUrl: 'avocado.png',
                title: title,
                message: message
            },
            function(response) {
                console.log('Notify response:', response, chrome.runtime.lastError)
            }
        );
    });
}