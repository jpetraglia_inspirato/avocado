console.log('Aloha from all lightning!');

window.addEventListener("DOMContentLoaded", function() {
    var styleArray = [];
    // To add to the <style> tag, create a function that returns CSS & push it into styleArray; the rest is taken care of
    styleArray.push(substituteCustomIcon());

    addStyleTag(styleArray.join(' '));
    setTimeout(removeSecurityWarning, 500);
});

function substituteCustomIcon() {
    return '.slds-global-header__logo { background-image: url(' + chrome.runtime.getURL('sfinsp.png') + ') !important; }';
    
}

function addStyleTag(styleContent) {
    var styleElement = document.createElement('style');
    styleElement.type = 'text/css';
    if(styleElement.styleSheet)  
        styleElement.styleSheet.cssText = styleContent; 
    else  
        styleElement.appendChild(document.createTextNode(styleContent));
    document.getElementsByTagName("head")[0].appendChild(styleElement);
}


// NOTE: This may or may not be needed. Historically, it keeps returning every time you navigate to a new page, which is annoying, hence adding this.
var securityWarningAttemptsRemaining = 6;
function removeSecurityWarning() {
    securityWarningAttemptsRemaining--;
    var banner = document.querySelector('.system-message[data-message-id^="SecurityUpdates"]');
    console.log('removeSecurityWarning', banner);
    if(banner) {
        banner.remove();
    } else if(securityWarningAttemptsRemaining > 0) {
        setTimeout(removeSecurityWarning, 500);
    }
}