console.log('Aloha from the deployment watcher!');

var statusNeedsChecking = false;
var pollTimeSeconds = 5;
var lastFailureCount = 0;
setInterval(watchDeployment, pollTimeSeconds * 500);
// watchDeployment();

function watchDeployment() {
    var inProgressDeploymentHeader = document.getElementById('inProgressDeployHeader');
    console.log(inProgressDeploymentHeader);
    if(!inProgressDeploymentHeader) {
        console.debug('No header found');
        return;
    }
    var status = inProgressDeploymentHeader.textContent.replace('🥑', '');
    var failureCount = document.querySelector('.progressChartContainer .failureCount');
    console.log(failureCount, lastFailureCount);
    var moreFailuresThanBefore = false;
    if(failureCount) {
        failureCount = parseInt(failureCount.textContent);
        moreFailuresThanBefore = failureCount != lastFailureCount;
        lastFailureCount = failureCount;
    }
    console.log(moreFailuresThanBefore);
    if(!statusNeedsChecking && (status.includes('In Progress') || moreFailuresThanBefore)) {
        statusNeedsChecking = true;
    }
    inProgressDeploymentHeader.textContent = inProgressDeploymentHeader.textContent.replace('🥑', '').trim() + (statusNeedsChecking ? ' 🥑' : '');
    if((!status.includes('In Progress') || moreFailuresThanBefore) && statusNeedsChecking) {
        var orgName = location.href.split('//')[1].split('.')[0];
        if(orgName.includes("--")) {
            orgName = orgName.split('--')[1];
        }
        orgName = orgName.charAt(0).toUpperCase() + orgName.substring(1).toLowerCase();
        var changeSetName = document.getElementsByClassName('changesetLink')[0].textContent;
        console.debug('Fishy change set: ' + changeSetName);
        chrome.runtime.sendMessage({
            type: 'notify',
            orgName: orgName,
            changeSetName: changeSetName,
            status: status,
            failures: failureCount
        }, function(response){
            console.log('Fishy message response:', response, chrome.runtime.lastError);
        });
        statusNeedsChecking = false;
    }
}


/*TODO:
    pollTime config
    alert on failure (mid deployment) as well
    make page title better:
      - display progress (% or #)?
    check in background? (i.e. with no tab open)
      - have a "Notify me for this org" action in the button, and show a list of orgs being watched?
    pretty sure statusNeedsChecking isn't working at all, cause setting it to true before sendMessage breaks the notification


Does the manifest need `"https://*.lightning.force.com/lightning/setup/DeployStatus*"`?
*/